import cndprint


_level = "info"
_silent_mode = False
_print = cndprint.CndPrint(level=_level, silent_mode=_silent_mode)

_data_obj = {
    "name": "Abc"
}
_id = 123

class MyTest:
    def __init__(self, data):
        pass
        
    def save(self):
        return True
        
    def all():
        result = []
        for x in range(6):
            result.append(MyTest(_data_obj))
        return result
        
    def find_by_id(id):
        data_loaded = _data_obj
        return MyTest(data_loaded)
        
    def update(self, data):
        return True
        
    def destroy(self):
        return True
        
    def has_children(self):
        return True
        
    def find_relation(self):
        return True

