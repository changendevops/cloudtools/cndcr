from mockito import when, mock, unstub
from expects import *
from mamba import description, context, it
import cnd_cr
import tests.vars as vars


with description("CndCr") as self:
    with before.each:
        unstub()
        self.cnd_cr = cnd_cr.Cr(vars.MyTest, vars._print)
        
    with context('__init__'):
        with it('should set _print'):
            expect(self.cnd_cr._print).to(equal(vars._print))
            
    with context('new'):
        with it('should return True is create success'):
            result = self.cnd_cr.new(vars._data_obj)
            expect(result).to(equal(True))
            
    with context('get'):
        with it('should return object if object is found'):
            result = self.cnd_cr.get(vars._id)
            expect(isinstance(result, vars.MyTest)).to(equal(True))
            
    with context('find_by_id'):
        with it('should be an alias of get'):
            when(self.cnd_cr).get(...).thenReturn(None)
            result = self.cnd_cr.find_by_id(vars._id)
            expect(result).to(equal(None))
            
    with context('destroy'):
        with it('should destroy object if object is found'):
            result = self.cnd_cr.destroy(vars._id)
            expect(result).to(equal(True))
            
        with it('should return None if object is not existing'):
            when(self.cnd_cr).get(...).thenReturn(None)
            result = self.cnd_cr.destroy(vars._id)
            expect(result).to(equal(None))
            
        with it('should return False if destroy failed'):
            my_test = mock(vars.MyTest)
            when(my_test).destroy(...).thenReturn(False)
            when(self.cnd_cr).get(...).thenReturn(my_test)
            result = self.cnd_cr.destroy(vars._id)
            expect(result).to(equal(False))
            
    with context('update'):
        with it('should update object if object is found'):
            result = self.cnd_cr.update(vars._id, vars._data_obj)
            expect(result).to(equal(True))
            
        with it('should return None if object is not existing'):
            when(self.cnd_cr).get(...).thenReturn(None)
            result = self.cnd_cr.update(vars._id, vars._data_obj)
            expect(result).to(equal(None))
            
        with it('should return False if update failed'):
            my_test = mock(vars.MyTest)
            when(my_test).update(...).thenReturn(False)
            when(self.cnd_cr).get(...).thenReturn(my_test)
            result = self.cnd_cr.update(vars._id, vars._data_obj)
            expect(result).to(equal(False))
            
    with context('find_all'):
        with it('should find_all object'):
            result = self.cnd_cr.find_all()
            expect(isinstance(result, list)).to(equal(True))
            expect(isinstance(result[0], vars.MyTest)).to(equal(True))



