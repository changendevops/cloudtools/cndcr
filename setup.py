from setuptools import setup, find_packages
from cnd_cr.__version__ import (
    __version__,
)

with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(name='cnd_cr',
    version=__version__,
    description="Base for vro Custom Resource",
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent'
    ],
    keywords='',
    author='Denis FABIEN',
    author_email='denis.fabien@changendevops.com',
    url='https://gitlab.com/changendevops/cloudtools/cnd-cr.git',
    license='MIT/X11',
    packages=find_packages(exclude=['spec', 'spec.*']),
    include_package_data=True,
    package_data={'cnd_cr': ['VERSION']},
    zip_safe=False,
    install_requires=required,
    test_require=['expect', 'doublex', 'doublex-expects'],
    project_urls={
        "Documentation": "https://changendevops.com",
        "Source": "https://gitlab.com/changendevops/cloudtools/cnd-cr.git",
    },
)
